<?php

namespace AppBundle\Service;

class DaysService
{
    public $formatDate = 'Y-m-d';

    /**
     * @param int $dayWeek
     *
     * @return bool
     */
    private function isWorkingDay(int $dayWeek): bool
    {
        return $dayWeek != 0 && $dayWeek != 6;
    }

    /**
     * @param \DateTime $date
     *
     * @return int
     */
    private function getDayWeek(\DateTime $date): int
    {
        return intval($date->format(("w")));
    }

    /**
     * @param string $string
     *
     * @return \DateTime
     */
    private function formatStringToDate(string $string): \DateTime
    {
        return date_create_from_format($this->formatDate, $string);
    }

    /**
     * @param string $stringDateStart
     * @param string $stringDateFinish
     * @param array $arrayStringHoliday
     *
     * @return int
     */
    public function getWorkingDaysCount(
        string $stringDateStart,
        string $stringDateFinish,
        array $arrayStringHoliday = []
    ): int {
        $dateStart  = $this->formatStringToDate($stringDateStart);
        $dateFinish = $this->formatStringToDate($stringDateFinish);

        $workingDaysCount = floor(date_diff($dateFinish, $dateStart)->days / 7) * 5;

        $dayWeekStart  = $this->getDayWeek($dateStart);
        $dayWeekRunner = $dayWeekStart;
        $dayWeekFinish = $this->getDayWeek($dateFinish);
        do {
            if ($this->isWorkingDay($dayWeekRunner)) {
                $workingDaysCount++;
            }
            $dayWeekRunner = ($dayWeekRunner + 1) % 7;
        } while ($dayWeekRunner != ($dayWeekFinish + 1) % 7);

        foreach ($arrayStringHoliday as $stringHoliday) {
            $dateHoliday    = $this->formatStringToDate($stringHoliday);
            $dayWeekHoliday = $this->getDayWeek($dateHoliday);
            if ($this->isWorkingDay($dayWeekHoliday)) {
                $diffStart  = date_diff($dateHoliday, $dateStart);
                $diffFinish = date_diff($dateFinish, $dateHoliday);
                if ($diffStart->days >= 0 && $diffFinish->days >= 0) {
                    $workingDaysCount--;
                }
            }
        }

        return $workingDaysCount > 0 ? $workingDaysCount : 0;
    }
}
