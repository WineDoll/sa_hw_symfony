<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 18.03.2019
 * Time: 13:10
 */

namespace AppBundle\Service;

class FindService
{
    private $arrayFind;

    public function __construct(array $arr)
    {
        $this->arrayFind = $arr;
    }

    /**
     * @return array
     */
    public function findAll()
    {
        return array_unique($this->arrayFind, SORT_REGULAR);
    }

    /**
     * @param int $id
     *
     * @return string|null
     */
    public function findOneById(int $id)
    {
        return array_key_exists($id, $this->arrayFind) ? $this->arrayFind[$id] : null;
    }

    /**
     * @param string $slug
     *
     * @return string|null
     */
    public function findOneBySlug(string $slug)
    {
        $arrayTitle = array_map(function ($element) {
            return $element[0];
        }, $this->arrayFind);

        return in_array($slug, $arrayTitle) ? $slug : null;
    }
}
