<?php

namespace AppBundle\Command;

use AppBundle\Service\DaysService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WorkingDaysCommandComma extends Command
{
    protected $service;

    public function __construct(DaysService $service)
    {
        $this->service = $service;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName("app:wdays:comma")
            ->setDescription("Get number of working days in chosen period taking holidays into account")
            ->addArgument(
                'dateStart',
                InputArgument::REQUIRED,
                "Period start date in " . $this->service->formatDate . " format"
            )
            ->addArgument(
                'dateFinish',
                InputArgument::REQUIRED,
                "Period finish date in " . $this->service->formatDate . " format"
            )
            ->addArgument(
                'datesHoliday',
                InputArgument::OPTIONAL,
                "Holiday dates in " . $this->service->formatDate . " format separated by ','"
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $stringDateStart  = $input->getArgument('dateStart');
        $stringDateFinish = $input->getArgument('dateFinish');

        $stringHolidays = $input->getArgument('datesHoliday');
        if ($stringHolidays == null) {
            $output->writeln(
                "Number of working days in chosen period: "
                . $this->service->getWorkingDaysCount($stringDateStart, $stringDateFinish)
            );
        } else {
            $arrayStringHoliday = explode(",", $stringHolidays);
            $output->writeln(
                "Number of working days in chosen period excluding holidays: "
                . $this->service->getWorkingDaysCount($stringDateStart, $stringDateFinish, $arrayStringHoliday)
            );
        }
    }
}
