<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 01.04.2019
 * Time: 20:11
 */

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class LoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, [
                'label' => 'table.users.username'
            ])
            ->add('password', PasswordType::class, [
                'label' => 'table.users.password'
            ])
            ->add('remember_me', CheckboxType::class, [
                'label' => 'login.remember_me',
                'required' => false
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'button.submit'
            ]);
    }
}
