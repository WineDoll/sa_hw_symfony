<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 29.03.2019
 * Time: 17:06
 */

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                'label' => 'table.users.email',
                'constraints' => [
                    new Assert\Email(['message' => 'user.email.correctness']),
                    new Assert\NotBlank()
                ]
            ])
            ->add('userName', TextType::class, [
                'label' => 'table.users.username',
                'constraints' => [
                    new Assert\NotBlank(),
                    new Assert\Regex([
                        'pattern' => '/[ ]/',
                        'match' => false,
                        'message' => 'user.username.space'])
                ]
            ])
            ->add('firstName', TextType::class, [
                'required' => false,
                'label'    => 'table.users.firstname',
            ])
            ->add('lastName', TextType::class, [
                'required' => false,
                'label'    => 'table.users.lastname',
            ])
            ->add('password', PasswordType::class, [
                'required' => $options['passwordRequired'],
                'label'    => 'table.users.password',
                'constraints' => [
                    new Assert\Regex([
                        'pattern' => '/(^$|^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]{7,}$)/',
                        'message' => 'user.password.symbols'
                    ])
                ]
            ])
            ->add('role', ChoiceType::class, [
                'label'    => 'table.users.role',
                'choices'  => [
                    'table.users.role.admin' => User::ROLE_ADMIN,
                    'table.users.role.author' => User::ROLE_AUTHOR,
                    'table.users.role.user' => User::ROLE_USER,
                ],
            ])
            ->add('save', SubmitType::class, [
                'label' => 'button.submit',
            ])
            ->getForm();
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'passwordRequired' => true,
        ));
    }
}
