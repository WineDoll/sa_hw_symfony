<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 29.03.2019
 * Time: 16:56
 */

namespace AppBundle\Form;

use AppBundle\Entity\User;
use AppBundle\Validator\Constraint\NotWeekendConstraint;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class PostType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label'       => 'table.posts.title',
                'constraints' => [
                    new Assert\NotBlank(['message' => 'post.title.notblank']),
                    new Assert\Length([
                        'min'        => 10,
                        'minMessage' => 'post.title.length.min',
                    ]),
                ],
            ])
            ->add('slug', TextType::class, [
                'label'       => 'table.posts.slug',
                'constraints' => [
                    new Assert\NotBlank(['message' => 'post.slug.notblank']),
                    new Assert\Regex([
                        'pattern' => '/[^a-z0-9-]+/',
                        'match'   => false,
                        'message' => 'post.slug.symbols',
                    ]),
                ],
            ])
            ->add('description', TextType::class, [
                'required' => false,
                'label'    => 'table.posts.description',
            ])
            ->add('postAt', DateTimeType::class, [
                'label'       => 'table.posts.postat',
                'empty_data'  => array('------'),
                'required'    => false,
                'constraints' => [
                    new Assert\GreaterThan([
                        'value'   => 'now',
                        'message' => 'post.postat.blank_or_later',
                    ]),
                ],
            ]);
        if ($options['byAdmin']) {
            $builder->add('author', EntityType::class, [
                'class'        => User::class,
                'choice_label' => 'userName',
                'label'        => 'table.posts.author',
            ]);
        }
        $builder->add('save', SubmitType::class, [
            'label' => 'button.submit',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'byAdmin' => true,
        ));
    }
}
