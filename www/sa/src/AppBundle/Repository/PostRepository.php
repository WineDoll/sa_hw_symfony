<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 28.03.2019
 * Time: 0:48
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Post;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class PostRepository
{
    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Post::class);
    }

    /**
     * @param int $id
     *
     * @return Post|null
     */
    public function find(int $id)
    {
        return $this->repository->find($id);
    }

    /**
     * @return Post[]
     */
    public function findAll() : iterable
    {
        return $this->repository->findAll();
    }

    /**
     * @return Post[]
     */
    public function findAllPosted() : iterable
    {
        return $this
            ->repository
            ->createQueryBuilder('p')
            ->select('p')
            ->where('p.postAt < :currentDateTime')
            ->orWhere('p.postAt IS NULL')
            ->setParameter('currentDateTime', date_create())
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $slug
     *
     * @return Post|null
     */
    public function findOneBySlug(string $slug)
    {
        return $this->repository->findOneBy(['slug' => $slug]);
    }

    /**
     * @param string $slug
     *
     * @return Post[]
     */
    public function findByCategory(string $slug) : iterable
    {
        return $this
            ->repository
            ->createQueryBuilder('p')
            ->select('p')
            ->where('p.slug LIKE :slug')
            ->setParameter('slug', '%'.addcslashes($slug, '%_').'%')
            ->getQuery()
            ->getResult();
    }
}
