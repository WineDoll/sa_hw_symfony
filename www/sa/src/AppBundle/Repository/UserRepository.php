<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 28.03.2019
 * Time: 0:48
 */

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;

class UserRepository
{
    /**
     * @var EntityRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(User::class);
    }

    /**
     * @param int $id
     *
     * @return User|null
     */
    public function find(int $id)
    {
        return $this->repository->find($id);
    }

    /**
     * @return User[]
     */
    public function findAll()
    {
        return $this->repository->findAll();
    }
}
