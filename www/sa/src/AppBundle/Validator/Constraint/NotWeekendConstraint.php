<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 29.03.2019
 * Time: 21:37
 */

namespace AppBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotWeekendConstraint extends Constraint
{
    public $message = 'post.check.weekend.true';
}
