<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 29.03.2019
 * Time: 21:40
 */

namespace AppBundle\Validator\Constraint;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class NotWeekendConstraintValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (! $constraint instanceof NotWeekendConstraint) {
            throw new UnexpectedTypeException($constraint, NotWeekendConstraint::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (gettype($value) == \DateTime::class) {
            throw new UnexpectedTypeException($value, \DateTime::class);
        }

        $dayOfWeek = intval($value->format('w'));

        if ($dayOfWeek == 6 | $dayOfWeek == 0) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }
}
