<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 18.03.2019
 * Time: 11:57
 */

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\UserType;
use AppBundle\Repository\UserRepository;
use AppBundle\Service\FindService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(
 *     path="/{_locale}",
 *     name="user_",
 *     requirements={
 *          "_locale" : "en|ru"
 *     },
 *     host="%subdomain_admin%.%domain%"
 * )
 * @Security("has_role('ROLE_ADMIN')")
 * Class UserController
 * @package AppBundle\Controller
 */
class UserController extends Controller
{
    private $service;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(FindService $service, UserRepository $userRepository)
    {
        $this->service        = $service;
        $this->userRepository = $userRepository;
    }

    /**
     * @Route(
     *     path="/{id}",
     *     name="show",
     *     methods={"GET"},
     *     requirements={
     *          "id" : "\d+"
     *     }
     * )
     * @param $id
     *
     * @return Response
     */
    public function showAction($id)
    {
        $user = $this->userRepository->find($id);
        if (!is_null($user)) {
            return $this->render('users/show.html.twig', ['user' => $user]);
        } else {
            throw $this->createNotFoundException("User with id $id not found");
        }
    }

    /**
     * @Route(
     *     path="/new",
     *     name="new",
     *     methods={"GET","POST"},
     *     defaults={
     *         "id" : "1",
     *         "new" : "true"
     *     }
     * )
     * @Route(
     *     path="/{id}/edit",
     *     name="edit",
     *     methods={"GET","POST"},
     *     requirements={
     *         "id" : "\d+"
     *     }
     * )
     * @param $id
     * @param Request $request
     * @param bool $new
     *
     * @return Response
     */
    public function editAction($id, Request $request, $new = false)
    {
        if ($new) {
            $user = new User();
        } else {
            $user = $this->userRepository->find($id);
        }

        $form = $this->createForm(UserType::class, $user, ['passwordRequired' => $new]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setUserName($form->get('userName')->getData());
            $user->setEmail($form->get('email')->getData());
            $user->setFirstName($form->get('firstName')->getData());
            $user->setLastName($form->get('lastName')->getData());
            $user->setRole($form->get('role')->getData());

            $password = $form->get('password')->getData();
            $passwordEncoded = $this->get("security.password_encoder")->encodePassword($user, $password);
            if ($new) {
                $user->setPassword($passwordEncoded);
                $em->persist($user);
            }
            if (!$new && !is_null($passwordEncoded)) {
                $user->setPassword($passwordEncoded);
            }

            $em->flush();

            return $this->redirectToRoute('user_index');
        }

        return $this->render('users/edit.html.twig', [
            'form' => $form->createView(),
            'new' => $new,
            'id' => $id
        ]);
    }

    /**
     * @Route(
     *     path="/{id}/delete",
     *     name="delete",
     *     methods={"DELETE"},
     *     requirements={
     *          "id" : "\d+"
     *     }
     * )
     * @param $id
     *
     * @return Response
     */
    public function deleteAction($id)
    {
        $user = $this->userRepository->find($id);
        if (is_null($user)) {
            throw $this->createNotFoundException("User with id $id not found");
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();
        $this->get('session')->getFlashBag()->add(
            'notification',
            [
                'id'         => 'users.%id%.deleted',
                'parameters' => [
                    '%id%' => $id,
                ]
            ]
        );

        return $this->redirectToRoute("user_index");
    }

    /**
     * @Route(
     *     path="/{page}/{pageId}",
     *     name="index",
     *     methods={"GET"},
     *     requirements={
     *          "page" : "[a-zA-Z]+",
     *          "pageId" : "\d+"
     *     }
     * )
     * @param string $page
     * @param int $pageId
     *
     * @return Response
     */
    public function indexAction($page = "start", $pageId = 1)
    {
        $users = $this->userRepository->findAll();

        return $this->render('users/index.html.twig', ['users' => $users]);
    }
}
