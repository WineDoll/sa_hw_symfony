<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 18.03.2019
 * Time: 12:09
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\PostType;
use AppBundle\Repository\PostRepository;
use AppBundle\Service\FindService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route(
 *     path="/{_locale}",
 *     name="post_",
 *     requirements={
 *          "_locale" : "en|ru"
 *     }
 * )
 * @Security("has_role('ROLE_USER')")
 * Class PostController
 * @package AppBundle\Controller
 */
class PostController extends Controller
{
    private $service;

    /**
     * @var PostRepository
     */
    private $postRepository;

    public function __construct(FindService $service, PostRepository $postRepository)
    {
        $this->service        = $service;
        $this->postRepository = $postRepository;
    }

    /**
     * @Route(
     *     path="/user/new",
     *     name="new",
     *     methods={"GET","POST"},
     *     defaults={
     *         "id" : "1",
     *         "new" : "true"
     *     },
     *     host="%domain%"
     * )
     * @Route(
     *     path="/user/{id}/edit",
     *     name="edit",
     *     methods={"GET","POST"},
     *     requirements={
     *         "id" : "\d+"
     *     },
     *     host="%domain%"
     * )
     * @Security("has_role('ROLE_AUTHOR')")
     * @param $id
     * @param Request $request
     * @param bool $new
     *
     * @return Response
     */
    public function editAction($id, Request $request, $new = false)
    {
        if ($new) {
            $post = new Post();
        } else {
            $post = $this->postRepository->find($id);
        }

        $form = $this->createForm(
            PostType::class,
            $post,
            [
                'byAdmin' => $this->getUser()->getRole() == User::ROLE_ADMIN
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em   = $this->getDoctrine()->getManager();
            $post = $form->getData();
            if (!$this->getUser()->getRole() == User::ROLE_ADMIN) {
                $post->setAuthor($this->getUser());
            }
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('post_index');
        }

        return $this->render('posts/edit.html.twig', [
            'form' => $form->createView(),
            'new' => $new,
            'id' => $id
        ]);
    }

    /**
     * @Route(
     *     path="/user/{id}",
     *     name="show",
     *     methods={"GET"},
     *     requirements={
     *          "id" : "\d+"
     *     },
     *     host="%domain%"
     * )
     * @param $id
     *
     * @return Response
     */
    public function showAction($id)
    {
        $post = $this->postRepository->find($id);
        if (!is_null($post)) {
            return $this->render('posts/show.html.twig', ['post' => $post]);
        } else {
            throw $this->createNotFoundException("Post with id $id not found");
        }
    }

    /**
     * @Route(
     *     path="/user/{id}/delete",
     *     name="delete",
     *     methods={"DELETE"},
     *     requirements={
     *          "id" : "\d+"
     *     },
     *     host="%domain%"
     * )
     * @Security("has_role('ROLE_ADMIN')")
     * @param $id
     *
     * @return Response
     */
    public function deleteAction($id)
    {
        $post = $this->postRepository->find($id);
        if (is_null($post)) {
            throw $this->createNotFoundException("Post with id $id not found");
        }
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->get('session')->getFlashBag()->add(
            'notification',
            [
                'id'         => 'posts.%id%.deleted',
                'parameters' => [
                    '%id%' => $id,
                ]
            ]
        );

        return $this->redirectToRoute("post_index");
    }

    /**
     * @Route(
     *     path="/user/{page}/{pageId}",
     *     name="index",
     *     methods={"GET"},
     *     requirements={
     *          "page" : "\w+",
     *          "pageId" : "\d+"
     *     },
     *     host="%domain%"
     * )
     * @Security("has_role('ROLE_AUTHOR')")
     * @param string $page
     * @param int $pageId
     *
     * @return Response
     */
    public function indexAction($page = "start", $pageId = 1)
    {
        $posts = $this->postRepository->findAll();

        return $this->render('posts/index.html.twig', ['posts' => $posts, 'withActions' => true]);
    }

    /**
     * @Route(
     *     path="/post/{pageId}",
     *     name="list",
     *     methods={"GET"},
     *     requirements={
     *          "pageId" : "\d+"
     *     },
     *     host="%domain%"
     * )
     * @param int $pageId
     *
     * @return Response
     */
    public function listAction($pageId = 1)
    {
        $posts = $this->postRepository->findAllPosted();

        return $this->render('posts/index.html.twig', ['posts' => $posts, 'withActions' => false]);
    }

    /**
     * @Route(
     *     path="/post/{slug}",
     *     name="view",
     *     methods={"GET"},
     *     requirements={
     *          "slug" : "[a-zA-Z0-9\-]+"
     *     },
     *     host="%domain%"
     * )
     * @param $slug
     *
     * @return Response|NotFoundHttpException
     */
    public function viewAction($slug)
    {
        $post = $this->postRepository->findOneBySlug($slug);
        if (!is_null($post)) {
            return $this->render('posts/show.html.twig', ['post' => $post]);
        } else {
            return $this->createNotFoundException("Post with slug $slug not found");
        }
    }

    /**
     * @Route(
     *     path="/post/category/{slug}",
     *     name="category",
     *     methods={"GET"},
     *     requirements={
     *          "slug" : "[a-zA-Z0-9\-]+"
     *     },
     *     host="%domain%"
     * )
     * @param $slug
     *
     * @return Response
     */
    public function viewCategoryAction($slug)
    {
        $posts = $this->postRepository->findByCategory($slug);

        return $this->render('posts/index.html.twig', ['posts' => $posts, 'withActions' => false]);
    }

    /**
     * @Route(
     *     path="/post/search/category",
     *     name="search_category",
     *     methods={"GET"},
     *     host="%domain%"
     * )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function searchCategoryAction(Request $request)
    {
        $slug = $request->query->get('_search');
        return $this->redirectToRoute('post_category', ['slug' => $slug]);
    }
}
