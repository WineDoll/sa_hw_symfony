<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 31.03.2019
 * Time: 15:11
 */

namespace AppBundle\Controller;

use AppBundle\Form\LoginType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(
 *     path="/{_locale}",
 *     name="security_",
 *     requirements={
 *          "_locale" : "en|ru"
 *     }
 * )
 */
class SecurityController extends Controller
{
    /**
     * @Route("/login", name="login")
     * @param Request $request
     *
     * @return Response
     */
    public function loginAction(Request $request)
    {
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('main_index');
        } else {
            $authenticationUtils = $this->get('security.authentication_utils');
            $defaultData         = array('username' => $authenticationUtils->getLastUsername());
            $form                = $this->createForm(LoginType::class, $defaultData);
            $error               = $authenticationUtils->getLastAuthenticationError();
        }
        $form->handleRequest($request);

        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error'         => $error,
            'form'          => $form->createView(),
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logoutAction()
    {
        #    return $this->redirectToRoute('main_index');
    }
}
