<?php
/**
 * Created by PhpStorm.
 * User: Alexey
 * Date: 22.03.2019
 * Time: 16:32
 */

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\TemplateController;
use Symfony\Component\Routing\Annotation\Route;

class MainPageController extends TemplateController
{
    /**
     * @Route(
     *     path="/{_locale}",
     *     requirements={
     *          "_locale" : "en|ru"
     *     },
     *     name="main_index",
     *     host="%domain%",
     *     defaults={"template" : "base.hw.html.twig"}
     * )
     * Class MainPageController
     * @package AppBundle\Controller
     *
     * @param $template
     * @param null $maxAge
     * @param null $sharedAge
     * @param null $private
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($template, $maxAge = null, $sharedAge = null, $private = null)
    {
        return parent::templateAction($template, $maxAge, $sharedAge, $private);
    }
}
