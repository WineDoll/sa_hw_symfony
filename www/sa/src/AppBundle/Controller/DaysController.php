<?php

namespace AppBundle\Controller;

use AppBundle\Service\DaysService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DaysController
{
    /**
     * @Route(path="/wdays")
     * @param DaysService $daysService
     *
     * @return Response
     */
    public function getWorkingDaysCountAction(DaysService $daysService)
    {
        $stringDateStart    = "2019-03-31";
        $stringDateFinish   = "2020-04-06";
        $arrayStringHoliday = [
            "2019-03-30",
            "2019-04-05",
            "2019-04-07",
        ];
        $workingDaysCount   = $daysService->getWorkingDaysCount(
            $stringDateStart,
            $stringDateFinish,
            $arrayStringHoliday
        );

        return new Response($workingDaysCount);
    }
}
